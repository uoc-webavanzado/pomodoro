
var timeInterval;
const clock = document.querySelector('#clock');
const minutesSpan = clock.querySelector('.minutes');
const secondsSpan = clock.querySelector('.seconds');

//Pomodoro 
const buttonStart = document.querySelector('button[name="buttonStart"]');
const buttonStop = document.querySelector('button[name="buttonStop"]');
const buttonReset = document.querySelector('button[name="buttonReset"]');

buttonStart.addEventListener('click', function(e){
    console.log("BUTTON START");
    console.log("## ELSE ## click Start");
    e.preventDefault();
    clearInterval(timeInterval);
    startClock(1500000);
})

buttonStop.addEventListener('click', function(e){
    console.log("BUTTON STOP");
    e.preventDefault();
    clearInterval(timeInterval);
})

buttonReset.addEventListener('click', function(e){
    console.log("BUTTON Reset");
    e.preventDefault();
    clearInterval(timeInterval);
    minutesSpan.innerHTML = ('00');
    secondsSpan.innerHTML = ('00');
})


function startClock(endTimeMilliseconds){
    function updateCounter(){
        let resultado= timeRemaining(endTimeMilliseconds);
        minutesSpan.innerHTML = ('0'+resultado.minutes).slice(-2);
        secondsSpan.innerHTML = ('0'+resultado.seconds).slice(-2);

        if(resultado.total <= 0){
            clearInterval(timeInterval);
        }
    }
    updateCounter();
    timeInterval = setInterval(updateCounter, 1000);
}

let contador = 0;
function timeRemaining(endTimeMilliseconds){
    let t = endTimeMilliseconds - contador;
    contador = contador + 1000;

    let seconds = Math.floor(( t/1000)%60);
    let minutes = Math.floor((t/1000/60)%60);

    return{
        'total':t,
        'minutes':minutes,
        'seconds':seconds
    };
   
}